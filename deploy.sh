sshpass  -p $infra1 ssh -o StrictHostKeyChecking=no admin@infra1.hoggart.eu <<-'ENDSSH'   
   docker stop photoshow2
   docker rm photoshow2
   docker pull registry.gitlab.com/hogsim/photoshow:master
   docker run --name photoshow2 --restart=always -p 48370:80  -v /home/simon/WEB/PhotoShow:/var/www/html -v  /home/simon/WEB/data-photoshow:/opt/PhotoShow -d -i -t registry.gitlab.com/hogsim/photoshow:master
ENDSSH