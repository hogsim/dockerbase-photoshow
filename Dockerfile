FROM debian:wheezy
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq

RUN DEBIAN_FRONTEND=noninteractive apt-get install -yq \
        --no-install-recommends \
        ca-certificates \
        git-core \
        vim \
        less \
        nginx \
        php5-fpm \
        php5-gd \
        libgd2-xpm \
        supervisor

RUN sed -i -e 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisor/supervisord.conf
ADD supervisor-photoshow.conf /etc/supervisor/conf.d/photoshow.conf

RUN mkdir -p /var/www/PhotoShow
RUN mkdir -p /opt/PhotoShow/Photos
RUN mkdir -p /opt/PhotoShow/generated
RUN chown -R www-data:www-data /opt/PhotoShow/Photos /opt/PhotoShow/generated

ADD fpm-photoshow.conf /etc/php5/fpm/pool.d/photoshow.conf
RUN sed -i -e 's/^.\+daemonize.\+$/daemonize = no/' /etc/php5/fpm/php-fpm.conf

ADD photoshow.nginx /etc/nginx/sites-available/photoshow
RUN rm -f /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/photoshow /etc/nginx/sites-enabled/photoshow
RUN echo "daemon off;" >> /etc/nginx/nginx.conf


VOLUME ["/opt/PhotoShow", "/var/log"]
EXPOSE 80
CMD /usr/bin/supervisord
